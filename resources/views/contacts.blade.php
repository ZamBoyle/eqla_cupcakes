<x-layout>
    <x-slot name="title">Contacts </x-slot>
    
    <div class="container bg-light mt-2 border rounded">
        <h1 class="text-center">Contacts</h1>
        <p>Nous sommes joignable via les plateformes suivantes:</p>
        <p><i class="bi bi-facebook" aria-label="facebook"></i> <i class="bi bi-twitter" aria-label="twitter"></i> <i class="bi bi-instagram" aria-label="instagram"></i> <i class="bi bi-linkedin" aria-label="linkedin"></i> <i class="bi bi-twitch" aria-label="twitch"></i> <i class="bi bi-discord" aria-label="discord"></i></p>
    </div>
</x-layout>